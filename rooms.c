/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rooms.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 17:55:53 by lpousse           #+#    #+#             */
/*   Updated: 2016/11/24 19:10:30 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static t_room	*new_room(char *name, int x, int y)
{
	t_room	*new;

	new = (t_room *)malloc(sizeof(t_room));
	if (new == NULL)
		error(errno);
	new->name = name;
	new->x = x;
	new->y = y;
	new->paths = NULL;
	new->nbpaths = 0;
	new->parent = NULL;
	new->dist = -1;
	new->visited = 0;
	new->pathed = 0;
	return (new);
}

static char		*get_data(t_anthill *ah, char *line, int *x, int *y)
{
	char	*name;
	int		namelen;
	int		i;

	namelen = *x;
	*x = ft_atoi(line + namelen);
	i = namelen + 1;
	while (line[i] != ' ')
		i++;
	*y = ft_atoi(line + i);
	name = ft_strndup(line, namelen);
	if (name == NULL)
		error(errno);
	i = -1;
	while (++i < ah->nbrooms)
	{
		if (ft_strcmp((ah->rooms[i])->name, name) == 0
			|| ((ah->rooms[i])->x == *x && (ah->rooms[i])->y == *y))
		{
			ft_strdel(&name);
			return (NULL);
		}
	}
	return (name);
}

int				add_room(t_anthill *ah, char *line, int namelen, char *cmd)
{
	t_room	**new;
	char	*name;
	int		x;
	int		y;
	int		i;

	x = namelen;
	name = get_data(ah, line, &x, &y);
	if (name == NULL)
		return (-1);
	new = (t_room **)malloc(sizeof(t_room *) * (ah->nbrooms + 1));
	i = -1;
	while (++i < ah->nbrooms)
		new[i] = ah->rooms[i];
	new[i] = new_room(name, x, y);
	if (cmd[START])
		ah->start = new[i];
	if (cmd[END])
		ah->end = new[i];
	if (ah->rooms != NULL)
		free(ah->rooms);
	ah->rooms = new;
	(ah->nbrooms)++;
	return (0);
}

void			delete_room(t_room **rooms)
{
	if (rooms == NULL || *rooms == NULL)
		return ;
	if ((*rooms)->name != NULL)
		ft_strdel(&((*rooms)->name));
	if ((*rooms)->paths != NULL)
		free((*rooms)->paths);
	free(*rooms);
	*rooms = NULL;
}
