/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   options_and_free.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/24 18:03:50 by lpousse           #+#    #+#             */
/*   Updated: 2016/11/24 18:37:14 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void		free_data(t_anthill *ah, char **data)
{
	int		i;

	ft_strdel(data);
	i = 0;
	while (i < ah->nbrooms)
	{
		delete_room(&(ah->rooms[i]));
		i++;
	}
	i = 0;
	while (i < ah->nbroads)
	{
		delete_roomlist(&(ah->roads[i]));
		i++;
	}
}

void		options(int ac, char **av)
{
	if (ac == 2 && ft_strcmp(av[1], "-h") == 0)
	{
		ft_putendl("usage:\n  ./lem-in [-ph] [< file]");
		ft_putendl("\n  -h: help\n  -p: show found paths");
		exit(0);
	}
	else if (ac != 2 || ft_strcmp(av[1], "-p") != 0)
	{
		ft_putendl("wrong flag");
		ft_putendl("usage:\n  ./lem-in [-ph] [< file]");
		ft_putendl("\n  -h: help\n  -p: show found paths");
		exit(0);
	}
}
