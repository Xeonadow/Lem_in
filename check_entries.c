/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_entries.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 18:53:33 by lpousse           #+#    #+#             */
/*   Updated: 2016/11/24 19:11:51 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static int	check_coord(char *line, int start, int end)
{
	if (line[start] == '+' || line[start] == '-')
		start++;
	if (ft_isdigit(line[start]) == 0)
		return (-1);
	while (start < end)
	{
		if (ft_isdigit(line[start]) == 0)
			return (-1);
		start++;
	}
	return (0);
}

int			check_room_desc(char *line)
{
	int		len;
	int		i;

	len = ft_strlen(line);
	i = len - 1;
	while (i > 0 && line[i] != ' ')
		i--;
	if (check_coord(line, i + 1, len) < 0)
		return (-1);
	len = i;
	i = len - 1;
	while (i > 0 && line[i] != ' ')
		i--;
	if (check_coord(line, i + 1, len) < 0 || i == 0)
		return (-1);
	return (i);
}

static int	rooms_exist(char *line, int i, t_room **rooms, t_anthill *ah)
{
	int		j;
	char	*first;

	j = 0;
	rooms[0] = NULL;
	rooms[1] = NULL;
	first = ft_strndup(line, i);
	while (j < ah->nbrooms && (rooms[0] == NULL || rooms[1] == NULL))
	{
		if (ft_strcmp(((ah->rooms)[j])->name, first) == 0)
			rooms[0] = (ah->rooms)[j];
		if (ft_strcmp(((ah->rooms)[j])->name, line + i + 1) == 0)
			rooms[1] = (ah->rooms)[j];
		j++;
	}
	ft_strdel(&first);
	if (rooms[0] != NULL && rooms[1] != NULL && rooms[0] != rooms[1])
		return (0);
	return (-1);
}

static int	add_path(t_room **rooms)
{
	t_room	**new;
	int		i;
	int		r;

	i = -1;
	while ((rooms[0])->nbpaths > 0 && ++i < (rooms[0])->nbpaths)
		if (((rooms[0])->paths)[i] == rooms[1])
			return (1);
	r = 0;
	while (r < 2)
	{
		new = (t_room **)malloc(sizeof(t_room *) * ((rooms[r])->nbpaths + 1));
		if (new == NULL)
			error(errno);
		i = -1;
		while (++i < (rooms[r])->nbpaths)
			new[i] = (rooms[r])->paths[i];
		new[i] = rooms[1 - r];
		if ((rooms[r])->paths != NULL)
			free((rooms[r])->paths);
		(rooms[r])->paths = new;
		((rooms[r])->nbpaths)++;
		r++;
	}
	return (0);
}

int			check_path_desc(t_anthill *ah, char *line, int len)
{
	t_room	*rooms[2];
	int		ret;
	int		i;

	ret = -1;
	i = -1;
	while (++i < len)
	{
		if (line[i] == '-' && rooms_exist(line, i, rooms, ah) >= 0)
		{
			if (((rooms[0] == ah->start && rooms[1] == ah->end) ||
				(rooms[1] == ah->start && rooms[0] == ah->end)) && !(ah->roads))
			{
				if (!(ah->roads = (t_roomlist **)malloc(sizeof(t_roomlist *))))
					error(errno);
				(ah->roads)[0] = new_roomlist(ah->start);
				add_at_start(&((ah->roads)[0]), new_roomlist(ah->end));
				(ah->nbroads)++;
			}
			else if ((ret = add_path(rooms)) == 0)
				return (0);
		}
	}
	return (ret);
}
