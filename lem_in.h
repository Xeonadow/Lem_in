/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/07 19:35:46 by lpousse           #+#    #+#             */
/*   Updated: 2016/11/24 19:11:40 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H

# include <stdlib.h>
# include <errno.h>
# include "libft.h"

# define NBCMD 2
# define START 0
# define END 1

# define NOROAD -1
# define NOSORE -2

typedef struct	s_room
{
	char			*name;
	struct s_room	**paths;
	int				nbpaths;
	struct s_room	*parent;
	int				x;
	int				y;
	int				dist;
	int				visited;
	int				pathed;
}				t_room;

typedef struct	s_roomlist
{
	t_room				*room;
	int					len;
	struct s_roomlist	*next;
}				t_roomlist;

typedef struct	s_anthill
{
	t_room		**rooms;
	int			nbrooms;
	t_roomlist	**roads;
	int			nbroads;
	t_room		*start;
	t_room		*end;
	int			ants;
}				t_anthill;

char			*parsing();
int				check_room_desc(char *line);
int				check_path_desc(t_anthill *ah, char *line, int len);

int				add_room(t_anthill *ah, char *line, int namelen, char *cmd);
void			delete_room(t_room **rooms);

t_roomlist		*new_roomlist(t_room *room);
void			delete_roomlist(t_roomlist **list);
void			add_at_end(t_roomlist **list, t_roomlist *new);
void			add_at_start(t_roomlist **list, t_roomlist *new);
int				roomlist_len(t_roomlist *list);

void			display_roads(t_roomlist **roads, int nbroads, t_roomlist *r);
void			display_anthill(t_anthill *ah);
void			display_ants(t_anthill *ah);
void			options(int ac, char **av);

void			free_data(t_anthill *ah, char **data);
void			error(int errcode);

#endif
