/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   roomlist.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/16 14:49:45 by lpousse           #+#    #+#             */
/*   Updated: 2016/11/22 20:43:31 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

t_roomlist	*new_roomlist(t_room *room)
{
	t_roomlist	*new;

	new = (t_roomlist *)malloc(sizeof(t_roomlist));
	if (new == NULL)
		error(errno);
	new->room = room;
	new->len = 0;
	new->next = NULL;
	return (new);
}

void		delete_roomlist(t_roomlist **list)
{
	t_roomlist	*tmp;

	if (list == NULL)
		return ;
	while (*list != NULL)
	{
		tmp = (*list)->next;
		free(*list);
		*list = tmp;
	}
}

void		add_at_end(t_roomlist **list, t_roomlist *new)
{
	t_roomlist	*tmp;

	if (list == NULL)
		return ;
	if (*list == NULL)
	{
		*list = new;
		return ;
	}
	tmp = *list;
	while (tmp->next != NULL)
		tmp = tmp->next;
	tmp->next = new;
}

void		add_at_start(t_roomlist **list, t_roomlist *new)
{
	if (list != NULL && new != NULL)
	{
		new->next = *list;
		*list = new;
	}
}

int			roomlist_len(t_roomlist *list)
{
	int		i;

	i = 0;
	while (list != NULL)
	{
		i++;
		list = list->next;
	}
	return (i);
}
