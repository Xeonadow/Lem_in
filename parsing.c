/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/07 19:37:58 by lpousse           #+#    #+#             */
/*   Updated: 2016/11/24 19:11:03 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static int	cmd_and_com(int step, char *line, char *cmd)
{
	if (line[1] == '#')
	{
		if (ft_strcmp(line, "##start") == 0)
			cmd[START] = 1;
		else if (ft_strcmp(line, "##end") == 0)
			cmd[END] = 1;
	}
	return (step);
}

static int	rooms_and_paths(t_anthill *ah, int step, char *line, char *cmd)
{
	int		i;
	int		len;

	i = 0;
	if (step == 1)
	{
		if ((len = check_room_desc(line)) < 0)
			step = 2;
		else if (add_room(ah, line, len, cmd) < 0)
			return (-1);
	}
	if (step == 2)
	{
		if (check_path_desc(ah, line, ft_strlen(line)) < 0)
			return (-1);
	}
	return (step);
}

static int	check_data(t_anthill *ah, int step, char *line, char *cmd)
{
	int		i;

	if (line[0] == '#')
		return (cmd_and_com(step, line, cmd));
	else if (step == 0)
	{
		i = ft_strlen(line);
		while (--i >= 0)
			if (ft_isdigit(line[i]) == 0)
				return (-1);
		ah->ants = ft_atoi(line);
		if (ah->ants <= 0)
			return (-1);
		ft_bzero(cmd, NBCMD);
		return (1);
	}
	step = rooms_and_paths(ah, step, line, cmd);
	ft_bzero(cmd, NBCMD);
	return (step);
}

static char	*join_data(char **data, char **line)
{
	char	*new;

	if (*data == NULL)
		return (*line);
	new = ft_strnew(ft_strlen(*data) + ft_strlen(*line) + 1);
	ft_strcpy(new, *data);
	ft_strcat(new, "\n");
	ft_strcat(new, *line);
	ft_strdel(data);
	ft_strdel(line);
	return (new);
}

char		*parsing(t_anthill *anthill)
{
	char	*line;
	int		step;
	char	cmd[NBCMD];
	char	*data;

	step = 0;
	data = NULL;
	ft_bzero(cmd, NBCMD);
	while (ft_gnl(0, &line) >= 0)
	{
		if (line == NULL)
			error(errno);
		step = check_data(anthill, step, line, cmd);
		if (step < 0)
		{
			ft_strdel(&line);
			return (data);
		}
		data = join_data(&data, &line);
	}
	return (data);
}
