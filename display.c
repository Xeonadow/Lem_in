/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 15:21:49 by lpousse           #+#    #+#             */
/*   Updated: 2016/11/24 19:05:43 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void		display_roads(t_roomlist **roads, int nbroads, t_roomlist *road)
{
	int			i;

	i = 0;
	if (road != NULL)
	{
		display_roads(NULL, 0, road->next);
		ft_printf("%s - ", (road->room)->name);
	}
	else if (roads != NULL)
	{
		ft_putendl("paths found :");
		while (i < nbroads)
		{
			ft_putstr("  ");
			display_roads(NULL, 0, (roads[i])->next);
			ft_putendl(((roads[i])->room)->name);
			i++;
		}
		ft_putchar('\n');
	}
}

void		display_anthill(t_anthill *ah)
{
	int		i;
	int		j;
	t_room	*room;

	ft_printf("ants: %d\nnbrooms: %d\nrooms:\n", ah->ants, ah->nbrooms);
	i = 0;
	while (i < ah->nbrooms)
	{
		room = ah->rooms[i];
		ft_printf("  name: %s, x: %d, y : %d, nbpaths: %d\n",
					room->name, room->x, room->y, room->nbpaths);
		ft_putstr("    linked to: ");
		j = -1;
		while (++j < room->nbpaths)
			ft_printf("%s ", (room->paths[j])->name);
		ft_putchar('\n');
		i++;
	}
	if (ah->start != NULL)
		ft_printf("start: %s\n", ah->start->name);
	if (ah->end != NULL)
		ft_printf("end: %s\n", ah->end->name);
	ft_putchar('\n');
}

static int	should_pick_ant(t_roomlist **roads, int j, int ants_left)
{
	int			k;
	int			total;

	k = 0;
	total = 0;
	while (k < j)
	{
		total += (roads[j])->len - (roads[k])->len;
		k++;
	}
	return (ants_left > total);
}

static void	road_progress(t_anthill *ah, t_roomlist *road, int j, int *i)
{
	int		ant;

	while (road->next != NULL)
	{
		if ((road->next)->room == ah->start)
		{
			if (should_pick_ant(ah->roads, j, ah->ants - *i))
				ant = ++(*i);
			else
				ant = 0;
		}
		else
			ant = ((road->next)->room)->visited;
		if (ant != 0)
		{
			if (road->room == ah->end)
				((ah->end)->visited)++;
			else
				(road->room)->visited = ant;
			ft_printf("L%d-%s ", ant, (road->room)->name);
			((road->next)->room)->visited = 0;
		}
		road = road->next;
	}
}

void		display_ants(t_anthill *ah)
{
	int		i;
	int		j;

	i = -1;
	while (++i < ah->nbrooms)
		((ah->rooms)[i])->visited = 0;
	i = -1;
	while (++i < ah->nbroads)
		((ah->roads)[i])->len = roomlist_len((ah->roads)[i]);
	i = 0;
	while (ah->end->visited < ah->ants)
	{
		j = -1;
		while (++j < ah->nbroads)
			road_progress(ah, (ah->roads)[j], j, &i);
		ft_putchar('\n');
	}
}
