/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/07 19:37:11 by lpousse           #+#    #+#             */
/*   Updated: 2016/11/24 19:16:27 by lpousse          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void		error(int errcode)
{
	if (errcode > 0)
		perror("ERROR:");
	else
		ft_putendl_fd("ERROR", 2);
	exit(0);
}

static void	add_to_queue(t_roomlist **queue, t_room *room, int nbvisit)
{
	t_room	*cur;
	int		i;

	i = 0;
	while (i < room->nbpaths)
	{
		cur = (room->paths)[i];
		if (cur->pathed == 0 && cur->visited < nbvisit)
		{
			cur->visited = nbvisit;
			cur->parent = room;
			add_at_end(queue, new_roomlist(cur));
		}
		i++;
	}
}

static void	add_road(t_anthill *ah)
{
	t_roomlist	**new;
	t_roomlist	*road;
	t_room		*cur;
	int			i;

	cur = ah->end;
	road = NULL;
	while (cur != NULL)
	{
		add_at_end(&road, new_roomlist(cur));
		if (cur != ah->end)
			cur->pathed = 1;
		cur = cur->parent;
	}
	new = (t_roomlist **)malloc(sizeof(t_roomlist *) * (ah->nbroads + 1));
	if (new == NULL)
		error(errno);
	i = -1;
	while (++i < ah->nbroads)
		new[i] = ah->roads[i];
	if (ah->roads != NULL)
		free(ah->roads);
	new[i] = road;
	ah->roads = new;
	(ah->nbroads)++;
}

static int	find_road(t_anthill *ah, int nbvisit)
{
	t_roomlist	*queue;
	t_roomlist	*current;

	if (ah->start == NULL || ah->end == NULL)
		error(NOSORE);
	queue = new_roomlist(ah->start);
	current = queue;
	(ah->start)->visited = nbvisit;
	while (current != NULL && current->room != ah->end)
	{
		add_to_queue(&current, current->room, nbvisit);
		if ((current->room)->parent == NULL)
			(current->room)->dist = 0;
		else
			(current->room)->dist = ((current->room)->parent)->dist + 1;
		current = current->next;
	}
	if (current == NULL)
	{
		delete_roomlist(&queue);
		return (-1);
	}
	delete_roomlist(&queue);
	add_road(ah);
	return (0);
}

int			main(int ac, char **av)
{
	t_anthill	anthill;
	int			i;
	char		*data;

	anthill.rooms = NULL;
	anthill.start = NULL;
	anthill.end = NULL;
	anthill.nbrooms = 0;
	anthill.roads = NULL;
	anthill.nbroads = 0;
	if (ac > 1)
		options(ac, av);
	data = parsing(&anthill);
	i = 1;
	while (find_road(&anthill, i) >= 0)
		i++;
	if (anthill.nbroads == 0)
		error(NOROAD);
	ft_printf("%s\n\n", data);
	if (ac == 2 && ft_strcmp(av[1], "-p") == 0)
		display_roads(anthill.roads, anthill.nbroads, NULL);
	display_ants(&anthill);
	return (0);
}
